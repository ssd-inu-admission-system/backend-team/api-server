from flask_sqlalchemy import SQLAlchemy
from backend_application import db

from sqlalchemy.orm import backref


class Program(db.Model):
    """Program that applicant can apply to
    """

    __tablename__ = 'Program'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    program_name = db.Column(db.String, nullable=False)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Program(%(id)s)>' % self.__dict__


class Applicant(db.Model):
    """Person, who is applying to the university
        education_type:
        0 - school
        1 - university
    """

    __tablename__ = 'Applicant'

    id = db.Column(db.Integer, primary_key=True, nullable=False)

    email = db.Column(db.String, nullable=False)
    password_hash = db.Column(db.String, nullable=False)

    name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String, nullable=False)
    middle_name = db.Column(db.String, nullable=True)

    phone = db.Column(db.String, nullable=False)

    citizenship = db.Column(db.String, nullable=False)
    source = db.Column(db.String, nullable=False)

    education_type = db.Column(db.Integer, nullable=True)
    education_id = db.Column(db.Integer, nullable=True)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Applicant(%(id)s)>' % self.__dict__


class ApplicantInfo(db.Model):
    """Extended Information about Applicant
    gender:
        male if True, female if False
    """

    __tablename__ = 'ApplicantInfo'

    # I can do this with applicant_id as a primary key,
    # and same for education info, but honestly, I don't
    # think I have time or wish to bother with that
    id = db.Column(db.Integer, primary_key=True, nullable=False)

    applicant_id = db.Column(db.Integer, db.ForeignKey('Applicant.id'), nullable=False)
    # uselist preserves this as one-to-one relationship
    applicant = db.relationship('Applicant', backref=db.backref('applicant_info', uselist=False))

    birth_date = db.Column(db.Date, nullable=False)
    gender = db.Column(db.Boolean, nullable=False)

    country_of_living = db.Column(db.String, nullable=False)
    city = db.Column(db.String, nullable=False)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<ApplicantInfo(%(id)s)>' % self.__dict__


class SchoolEducation(db.Model):
    """Data about education in the school
    """

    __tablename__ = "SchoolEducation"

    id = db.Column(db.Integer, primary_key=True, nullable=False)

    applicant_id = db.Column(db.Integer, db.ForeignKey('Applicant.id'), nullable=False)
    applicant = db.relationship('Applicant')

    # Education Info

    country_of_study = db.Column(db.String, nullable=False)
    city_of_study = db.Column(db.String, nullable=False)
    name_of_school = db.Column(db.String, nullable=False)
    grad_year = db.Column(db.Integer, nullable=False)
    avg_grade = db.Column(db.Float, nullable=False)

    russian_test_year = db.Column(db.Integer, nullable=True)
    russian_test_result = db.Column(db.Integer, nullable=True)

    math_test_year = db.Column(db.Integer, nullable=True)
    math_test_result = db.Column(db.Integer, nullable=True)

    infophysics_test_year = db.Column(db.Integer, nullable=True)
    infophysics_test_result = db.Column(db.Integer, nullable=True)

    diploma_date = db.Column(db.Date, nullable=True)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<SchoolEducation(%(id)s)>' % self.__dict__


class UniversityEducation(db.Model):
    """Data about education in the school
    """

    __tablename__ = "UniversityEducation"

    id = db.Column(db.Integer, primary_key=True, nullable=False)

    applicant_info_id = db.Column(db.Integer, db.ForeignKey('ApplicantInfo.id'), nullable=False)
    applicant_info = db.relationship('ApplicantInfo')

    # Education Info

    country_of_study = db.Column(db.String, nullable=False)
    city_of_study = db.Column(db.String, nullable=False)
    name_of_university = db.Column(db.String, nullable=False)
    name_of_program = db.Column(db.String, nullable=False)

    gpa = db.Column(db.Float, nullable=False)

    graduation_date = db.Column(db.Date, nullable=True)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<UniversityEducation(%(id)s)>' % self.__dict__


class Application(db.Model):
    """Application to a program by applicant
    Status can be:
    0 - applied
    1 - interview
    2 - approved
    3 - rejected
    """

    __tablename__ = 'Application'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    applicant_id = db.Column(db.Integer, db.ForeignKey('Applicant.id'), index=True, nullable=False)
    program_id = db.Column(db.Integer, db.ForeignKey('Program.id'), index=True, nullable=False)

    interviewer_id = db.Column(db.Integer, db.ForeignKey('Staff.id'), nullable=True)
    application_grade = db.Column(db.Integer, nullable=True)
    application_review = db.Column(db.String, nullable=True)

    status = db.Column(db.Integer, nullable=False, default=0)

    applicant = db.relationship('Applicant', backref='applications')
    program = db.relationship('Program', backref='applications')
    interviewer = db.relationship('Staff', backref='applications')

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Application(%(id)s)>' % self.__dict__

#
# Tests and Questions
#


"""Many-to-many mapping of tests that needed to be completed
to apply for a program
"""
tests_program_association_table = db.Table(
    'test_program_association',
    # db.Model,
    db.Column('program_id', db.Integer, db.ForeignKey('Program.id'), index=True, nullable=False),
    db.Column('test_id', db.Integer, db.ForeignKey('Test.id'), index=True, nullable=False)
)


class Test(db.Model):

    __tablename__ = 'Test'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    test_title = db.Column(db.String)
    description = db.Column(db.String)

    # programs that include given test
    programs = db.relationship('Program',
                               secondary=tests_program_association_table,
                               backref="tests")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Test(%(id)s)>' % self.__dict__


class Question(db.Model):
    """A question in the text.
    question_type may be:
    0. Open - unused
    1. Single-choice
    2. Multi-choice
    """

    __tablename__ = 'Question'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    test_id = db.Column(db.Integer, db.ForeignKey('Test.id'), nullable=False)

    question_text = db.Column(db.String, nullable=False)
    question_type = db.Column(db.Integer, nullable=False)

    # Used to sort questions in test
    question_index = db.Column(db.Integer, nullable=False)

    test = db.relationship('Test', backref=backref('questions', cascade='all, delete-orphan'))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Question(%(id)s)>' % self.__dict__


class QuestionOption(db.Model):
    """Option for a multi-choice or a single-choice questions
    """

    __tablename__ = 'QuestionOption'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('Question.id'), index=True, nullable=False)
    option_text = db.Column(db.String(45), nullable=False)
    is_correct = db.Column(db.Boolean, nullable=False)

    # Used to sort options in question
    option_index = db.Column(db.Integer, nullable=False)

    question = db.relationship('Question', backref=backref('options', cascade='all, delete-orphan'))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Questionoption(%(id)s)>' % self.__dict__


class TestAttempt(db.Model):
    """Test attempt by applicant. Although we only assume one attempt,
    it's easier to track them this way, and we store a grade here.
    """

    __tablename__ = 'TestAttempt'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    application_id = db.Column(db.Integer, db.ForeignKey('Application.id'), index=True, nullable=False)
    test_id = db.Column(db.Integer, db.ForeignKey('Test.id'), index=True, nullable=False)

    grade = db.Column(db.Float, nullable=True)

    application = db.relationship('Application', backref=backref('test_attempts', cascade='all, delete-orphan'))
    test = db.relationship('Test')


class QuestionChoice(db.Model):
    """Selected option in a multi- or single- choice question made by applicant
    """

    __tablename__ = 'QuestionChoice'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    test_attempt_id = db.Column(db.Integer, db.ForeignKey('TestAttempt.id'), index=True, nullable=False)

    question_id = db.Column(db.Integer, db.ForeignKey('Question.id'), index=True, nullable=False)
    question_option_id = db.Column(db.Integer, db.ForeignKey('QuestionOption.id'), index=True, nullable=False)

    test_attempt = db.relationship('TestAttempt', backref=backref('question_choices', cascade='all, delete-orphan'))
    question = db.relationship('Question')
    question_option = db.relationship('QuestionOption')

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Questionchoice(%(id)s)>' % self.__dict__

#
# Documents
#


class Document(db.Model):
    """Document for a program uploaded by applicant
    Document types:
    1 - CV/portfolio
    2 - Motivation Letter
    3 - Passport Scan
    4 - Transcripts
    5 - Recommendations
    6 - Project Description
    """

    __tablename__ = 'ApplicationDocument'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    application_id = db.Column(db.Integer, db.ForeignKey('Application.id'), index=True, nullable=False)
    document_type = db.Column(db.Integer, nullable=False)

    application = db.relationship('Application', backref='documents')

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<ApplicationDocument(%(id)s)>' % self.__dict__

#
# Staff
#


class Staff(db.Model):
    """Member of university staff. Permissions can be a:
    0 - superadministrator
    1 - manager
    2 - interviewer
    """

    __tablename__ = 'Staff'

    id = db.Column(db.Integer, primary_key=True, nullable=False)

    permissions = db.Column(db.Integer, nullable=False)

    full_name = db.Column(db.String)
    email = db.Column(db.String, unique=True, nullable=False)
    password_hash = db.Column(db.String, nullable=False)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if self.permissions == 0:
            return '<Superadministrator(%(id)s)>' % self.__dict__
        if self.permissions == 1:
            return '<Manager(%(id)s)>' % self.__dict__
        if self.permissions == 2:
            return '<Interviewer(%(id)s)>' % self.__dict__


""" Unused - we don't support those
class OpenQuestionAnswer(db.Model):
    \"""Answer to the Open Question made by applicant
    \"""

    __tablename__ = 'OpenQuestionAnswer'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    test_attempt_id = db.Column(db.Integer, db.ForeignKey('TestAttempt.id'), index=True, nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('Question.id'), index=True, nullable=False)
    answer_text = db.Column(db.String, nullable=False)

    test_attempt = db.relationship('TestAttempt', backref=backref('open_questions_answers', cascade='all, delete-orphan'))
    question = db.relationship('Question')

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<Openquestionanswer(%(id)s)>' % self.__dict__
"""

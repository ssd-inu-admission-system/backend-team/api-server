import os
import shutil

from backend_application import app, db
from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify, Blueprint, request, send_from_directory, abort
from werkzeug.utils import secure_filename

documents = Blueprint('documents', __name__, url_prefix='/documents')


def allowed_file(filename):
    """
    Check if a document has acceptable extension
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[-1].lower() in app.config['ALLOWED_EXTENSIONS']


@documents.route('/<int:application_id>/send', methods=['POST', ])
@role_required([-1])
def documents_send(info, application_id):
    """
    Send a document to the server as multipart/form-data.
    Currently accepted filename extensions: pdf, txt, doc, docx
    Can be performed only by users
    """
    response = {'status': -1, 'error_message': ''}

    try:
        application = Application.query.filter_by(id=application_id).first()

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    if application is None:
        response['error_message'] = 'No such application found'
        return jsonify(response)

    if info['user_id'] != application.applicant_id:
        response['error_message'] = 'Insufficient rights'
        return jsonify(response)

    if 'document' not in request.files:
        response['error_message'] = "No file sent"
        return jsonify(response)

    file = request.files['document']

    if file.filename == '':
        response['error_message'] = "No file selected"
        return jsonify(response)

    if not allowed_file(file.filename):
        response['error_message'] = "Filetype not supported"
        return jsonify(response)

    document_type = request.form.get('type', None)

    if document_type is None:
        response['error_message'] = "Expected field 'type' with value from 0 to 6"
        return jsonify(response)

    try:
        document_type = int(document_type)
    except ValueError:
        response['error_message'] = "Expected field 'type' to be numeral from 0 to 6"
        return jsonify(response)

    if document_type not in range(0, 7):
        response['error_message'] = "Expected field 'type' to have value from 0 to 6"
        return jsonify(response)

    try:
        # We want to replace the old document of the same type if we have any
        old_document = Document.query.filter_by(application_id=application_id, document_type=document_type).first()
        if old_document is not None:
            shutil.rmtree(f"{app.config['UPLOAD_FOLDER']}/{application_id}/{old_document.id}", ignore_errors=True)
            db.session.delete(old_document)
            db.session.commit()

        document = Document(application_id=application_id, document_type=document_type)

        db.session.add(document)
        db.session.commit()

        os.makedirs(f"{app.config['UPLOAD_FOLDER']}/{application_id}/{document.id}")
        file.save(os.path.join(f"{app.config['UPLOAD_FOLDER']}/{application_id}/{document.id}",
                               secure_filename(file.filename)))

        response['document_id'] = document.id
        response['status'] = 0

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@documents.route('/<int:application_id>/<int:document_id>', methods=['GET', ])
@role_required(None)
def documents_show(info, application_id, document_id):
    """
    Get the document related to the application
    """
    try:
        application = Application.query.filter_by(id=application_id).first()
    except SQLAlchemyError as e:
        return abort(500)

    if application is None:
        # I think "not found" describes the situation best?
        return abort(404)

    if info['role'] == -1 and info['user_id'] != application.applicant_id:
        return abort(401)

    try:
        document = Document.query.filter_by(id=document_id).first()

    except SQLAlchemyError as e:
        abort(404)

    if document is None:
        abort(404)

    path = f"{app.root_path}/{app.config['UPLOAD_FOLDER']}/{application_id}/{document.id}"
    # XXX: We have 1 file per folder. Should always work
    return send_from_directory(path, os.listdir(path)[0])


app.register_blueprint(documents)

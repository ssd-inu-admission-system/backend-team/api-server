from backend_application import app, db
from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify, Blueprint, request

applications = Blueprint('applications', __name__, url_prefix='/applications')


@applications.route('', methods=['GET', ])
@role_required([0, 1, 2])
def applications_list(info):
    """
    Get a list of application IDs
    Can be used by superadmin, managers and interviewers
    """
    response = {'status': -1, 'error_message': ''}

    try:
        applications = Application.query.filter_by().all()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['applications'] = []
        for i in applications:
            applicant = i.applicant
            application = dict(application_id=i.id, applicant_id=i.applicant_id, applicant_name=applicant.name,
                               applicant_surname=applicant.surname, application_status=i.status)
            response['applications'].append(application)

        response['status'] = 0

    return jsonify(response)


@applications.route('/<application_id>/info', methods=['GET', ])
@role_required(None)
def applications_info(info, application_id):
    """
    Get information about particular application
    Can be used by superadmin, managers and interviewers
    """
    response = {'status': -1, 'error_message': ''}

    try:
        application_info = Application.query.filter_by(id=application_id).first()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    if application_info is None:
        response['error_message'] = "No such application found"
        return jsonify(response)

    if info['role'] == -1 and info['user_id'] != application_info.applicant_id:
        response['error_message'] = 'Insufficient rights'
        return jsonify(response)

    tests = application_info.test_attempts
    # Frontend asked to send an empty array if there are no tests
    tests_info = []

    if len(tests) > 0:
        for i in tests:
            questions = i.test.questions
            test = dict(test_id=i.id, title=i.test.test_title, total=len(questions), grade=i.grade, correct=0)

            for question in questions:
                if question.question_type != 0:
                    correct_count = len([x for x in question.options if x.is_correct])

                    chosen_options = QuestionChoice.query.filter_by(test_attempt_id=i.id, question_id=question.id).all()
                    if chosen_options is not None:
                        for option in chosen_options:
                            if option.question_option.is_correct:
                                correct_count -= 1

                    if correct_count == 0:
                        test['correct'] += 1

            tests_info.append(test)

    document_list = application_info.documents
    documents = []

    if len(document_list) > 0:
        for d in document_list:
            document = {'document_id': d.id, 'document_type': d.document_type,
                        'document_url': f"documents/{application_id}/{d.id}"}

            documents.append(document)

    response['applicant_id'] = application_info.applicant_id
    response['application_status'] = application_info.status
    response['program_id'] = application_info.program_id
    response['tests_info'] = tests_info
    response['documents'] = documents

    interviewer = application_info.interviewer
    if interviewer is not None:
        response['interviewer_id'] = interviewer.id
    else:
        response['interviewer_id'] = None
    # Frontend asked to return these parameters even if no interviewer is attached.
    response['grade'] = application_info.application_grade
    response['comment'] = application_info.application_review

    response['status'] = 0

    return jsonify(response)


@applications.route('/create', methods=['POST', ])
@role_required([-1])
def applications_create(info):
    """
    Create an application
    Can be performed only by users
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /applications/create. Try checking the headers."
        return jsonify(response)

    if data.get('applicant_id', None) is None:
        response['error_message'] = "No data sent to /applications/create."
        return jsonify(response)

    try:
        # NOTE: program_id potentially temporary
        application = Application(program_id=1, applicant_id=data['applicant_id'])
        db.session.add(application)
        db.session.commit()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['id'] = application.id
        response['status'] = 0

    return jsonify(response)


@applications.route('/<int:application_id>/test_results', methods=['GET', ])
@role_required(None)
def applications_test_results(info, application_id):
    """
    Get test results from an application
    """
    response = {'status': -1, 'error_message': ''}

    try:
        application = Application.query.filter_by(id=application_id).first()

        if application is None:
            response['error_message'] = 'No application found'
            return jsonify(response)

        if info['role'] == -1 and info['user_id'] != application.applicant_id:
            response['error_message'] = 'Insufficient rights'
            return jsonify(response)

        test_attempts = application.test_attempts

        response['results'] = []

        if test_attempts is not None:
            for i in test_attempts:
                questions = i.test.questions
                result = dict(test_id=i.test_id, grade=i.grade, answers=[])

                for q in questions:
                    result['answers'].append({
                        # We need q to start from 0 here and 1 everywhere else
                        "number": q.question_index,
                        "answers": []
                    })

                    choices = QuestionChoice.query.filter_by(test_attempt=i, question=q)

                    if choices:
                        for choice in choices:
                            result['answers'][-1]['answers'].append(choice.question_option.option_index)

                response['results'].append(result)

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    response['status'] = 0

    return jsonify(response)


@applications.route('/<int:id>/change_status', methods=['POST', ])
@role_required([0, 1])
def applications_change_status(info, id):
    """
    Change status of an application
    Can be performed only by superadmin or manager
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /applications/change_status. Try checking the headers."
        return jsonify(response)

    if data.get('status', None) is None:
        response['error_message'] = "No status sent to /applications/change_status."
        return jsonify(response)

    if data['status'] < 0 or data['status'] > 4:
        response['error_message'] = "Incorrect status sent to /applications/change_status."
        return jsonify(response)

    try:
        application = Application.query.filter_by(id=id).first()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        application.status = data['status']
        db.session.commit()
        response['status'] = 0

    return jsonify(response)


@applications.route('/<application_id>/send_test', methods=['POST', ])
@role_required([-1])
def applications_send_test(info, application_id):
    """
    Send test results for an application
    Can be performed only by users
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /applications/send_test. Try checking the headers."
        return jsonify(response)

    try:
        application = Application.query.filter_by(id=application_id).first()

        if application is None:
            response['error_message'] = "No such application found"
            return jsonify(response)

        if info['role'] == -1 and info['user_id'] != application.applicant_id:
            response['error_message'] = 'Insufficient rights'
            return jsonify(response)

        test_id = data['test_id']

        test = Test.query.filter_by(id=test_id).first()
        if test is None:
            response['error_message'] = f"No test with application_id {test_id} was found in /applications/send_test"
            return jsonify(response)

        attempt = TestAttempt(application_id=application_id, test=test, grade=0.0)

        answers = data['answers']

        for a in answers:
            question = Question.query.filter_by(test_id=test_id, question_index=a['number']).first()
            for i in a['answers']:
                option = QuestionOption.query.filter_by(question=question, option_index=i).first()
                if option is None:
                    response['error_message'] = f'No such Question Option ({i}) found in Question {a["number"]}'
                    return jsonify(response)

                choice_answer = QuestionChoice(test_attempt=attempt, question=question,
                                               question_option=option)

                db.session.add(choice_answer)

        db.session.commit()

        # Grade test: (correct - incorrect) / total_correct * 100
        total_correct = QuestionOption.query \
            .filter(QuestionOption.question.has(test=attempt.test)) \
            .filter_by(is_correct=True) \
            .all()

        correct = QuestionChoice.query \
            .filter_by(test_attempt=attempt) \
            .filter(QuestionChoice.question_option.has(is_correct=True)) \
            .all()

        incorrect = QuestionChoice.query \
            .filter_by(test_attempt=attempt) \
            .filter(QuestionChoice.question_option.has(is_correct=False)) \
            .all()

        attempt.grade = max(0.0, ((len(correct) - len(incorrect)) / len(total_correct) * 100))

        response['status'] = 0
        db.session.commit()

    except KeyError:
        response['error_message'] = "Missing JSON fields"
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['status'] = 0

    return jsonify(response)


@applications.route('/<int:id>/grade', methods=['POST', ])
@role_required([2])
def applications_grade(info, id):
    """
    Add a grade to the application from 0 to 100
    Can be performed only by interviewer
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /applications/grade. Try checking the headers."
        return jsonify(response)

    if data.get('grade', None) is None:
        response['error_message'] = "No grade sent to /applications/grade."
        return jsonify(response)

    # We had some bug somewhere with the client-side sending strings instead of integers as needed
    # We tried many different things, but couldn't quite put the finger on the cause
    if type(data['grade']) is not int and not data['grade'].isdecimal():
        response['error_message'] = "Incorrect number sent"
        return jsonify(response)

    data['grade'] = int(data['grade'])

    if data['grade'] < 0 or data['grade'] > 100:
        response['error_message'] = "Incorrect grade sent to /applications/grade."
        return jsonify(response)

    try:
        application = Application.query.filter_by(id=id).first()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        application.application_grade = data['grade']
        db.session.commit()
        response['status'] = 0

    return jsonify(response)


@applications.route('/<int:id>/comment', methods=['POST', ])
@role_required([2])
def applications_comment(info, id):
    """
    Add a comment to the application
    Can be performed only by interviewer
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /applications/comment. Try checking the headers."
        return jsonify(response)

    if data.get('comment', None) is None:
        response['error_message'] = "No comment sent to /applications/comment."
        return jsonify(response)

    if data['comment'] == "":
        response['error_message'] = "Comment must not be empty"
        return jsonify(response)

    try:
        application = Application.query.filter_by(id=id).first()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        application.application_review = data['comment']
        db.session.commit()
        response['status'] = 0

    return jsonify(response)


app.register_blueprint(applications)

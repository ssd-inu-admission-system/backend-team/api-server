from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify, Blueprint, request

import datetime

users = Blueprint('users', __name__, url_prefix='/users')


@users.route('', methods=['GET', ])
@role_required([0, 1, 2])
def users_list(info):
    """
    Get a list of user IDs
    Can be used only by the staff
    """
    response = {'status': -1, 'error_message': ''}

    try:
        users = Applicant.query.all()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['users'] = []
        for i in users:
            user = dict(id=i.id, first_name=i.name, last_name=i.surname, middle_name=i.middle_name)
            response['users'].append(user)

        response['status'] = 0

    return jsonify(response)


@users.route('/sign_up', methods=['POST', ])
def users_sign_up():
    """
    Create a new user with the basic data
    Data is divided into 3 parts:
    Basic
    Complete
    Education
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /users/sign_up. Try checking the headers."
        return jsonify(response)

    try:
        user_new = Applicant(email=data["email"], password_hash=data["password_hash"],
                             name=data["first_name"], surname=data["last_name"],
                             citizenship=data["citizenship"], source=data["source"], phone=data["phone"])

        db.session.add(user_new)
        db.session.commit()

        token = generate_jwt_token(user_new.id, -1, datetime.timedelta(days=60))
        response['id'] = user_new.id
        response['token'] = token
        response['status'] = 0

    except KeyError:
        response['error_message'] = "JSON missing a required field"
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['status'] = 0
    finally:
        return jsonify(response)


@users.route('/<int:id>/update_info', methods=['POST', ])
@role_required([-1, 0, 1])
def users_update_info(info, id):
    """
    Update basic user info and add the complete information
    Data is divided into 3 parts:
    Basic
    Complete
    Education
    Can be used by the user themselves, the superadmin or the managers
    """
    response = {'status': -1, 'error_message': ''}

    if info['role'] == -1 and info['user_id'] != id:
        response['error_message'] = 'Insufficient rights'
        return jsonify(response)

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /users/update_info. Try checking the headers."
        return jsonify(response)

    try:
        user = Applicant.query.filter_by(id=id).first()
        if user is None:
            response['error_message'] = "No such user found"
            return jsonify(response)

        user.email = data['email']
        user.name = data['first_name']
        user.surname = data['last_name']
        user.citizenship = data['citizenship']
        user.source = data['source']
        user.phone = data['phone']
        user.middle_name = data.get('middle_name', None)

        user_info = user.applicant_info
        if user_info is None:
            user_info = ApplicantInfo(applicant=user, birth_date=datetime.date.fromisoformat(data['birth_date']),
                                      gender=data['gender'],
                                      country_of_living=data['country'], city=data['city'])
            db.session.add(user_info)
        else:
            user_info.birth_date = datetime.date.fromisoformat(data['birth_date'])
            user_info.gender = data['gender']
            user_info.country_of_living = data['country']
            user_info.city = data['city']

        db.session.commit()
    except KeyError:
        response['error_message'] = "JSON missing a required field"
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['status'] = 0
    finally:
        return jsonify(response)


@users.route('/<int:id>/update_education', methods=['POST', ])
@role_required([-1, 0, 1])
def users_update_education(info, id):
    """
    Update education info for a user
    Data is divided into 3 parts:
    Basic
    Complete
    Education
    Can be used by the user themselves, the superadmin or the managers
    """
    response = {'status': -1, 'error_message': ''}

    if info['role'] == -1 and info['user_id'] != id:
        response['error_message'] = 'Insufficient rights'
        return jsonify(response)

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /users/update_education. Try checking the headers."
        return jsonify(response)

    try:
        user = Applicant.query.filter_by(id=id).first()
        if user is None:
            response['error_message'] = "No such user found"
            return jsonify(response)

        date_diploma = data.get('date_diploma', None)
        if date_diploma is not None:
            date_diploma = datetime.date.fromisoformat(date_diploma)

        education = SchoolEducation.query.filter_by(id=user.education_id).first()

        if education is None:
            education = SchoolEducation(applicant=user, country_of_study=data['school_country'],
                                        city_of_study=data['school_city'],
                                        name_of_school=data['school_name'],
                                        grad_year=data['graduation_year'],
                                        avg_grade=data.get('grade_average', None),
                                        russian_test_year=data.get('year_russian', None),
                                        russian_test_result=data.get('grade_russian', None),
                                        math_test_year=data.get('year_math', None),
                                        math_test_result=data.get('grade_math', None),
                                        infophysics_test_year=data.get('year_info_physics', None),
                                        infophysics_test_result=data.get('grade_info_physics', None),
                                        diploma_date=date_diploma
                                        )
            db.session.add(education)
        else:
            education.country_of_study = data['school_country']
            education.city_of_study = data['school_city']
            education.name_of_school = data['school_name']
            education.grad_year = data['graduation_year']
            education.avg_grade = float(data.get('grade_average', None))
            education.russian_test_year = int(data.get('year_russian', None))
            education.russian_test_result = int(data.get('grade_russian', None))
            education.math_test_year = int(data.get('year_math', None))
            education.math_test_result = int(data.get('grade_math', None))
            education.infophysics_test_year = int(data.get('year_info_physics', None))
            education.infophysics_test_result = int(data.get('grade_info_physics', None))
            education.diploma_date = date_diploma

        db.session.commit()
        response['status'] = 0

    except KeyError:
        response['error_message'] = "JSON missing a required field"
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@users.route('/login', methods=['POST', ])
def users_login():
    """
    Log into the system with the user credentials
    Returns the access token with the id and the security permissions, along with the user name
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /users/login. Try checking the headers."
        return jsonify(response)

    try:
        user = Applicant.query.filter_by(email=data['email'], password_hash=data['password_hash']).first()

        if user is None:
            response['error_message'] = 'User not found'
        else:
            token = generate_jwt_token(user.id, -1, datetime.timedelta(days=60))
            response['first_name'] = user.name
            response['last_name'] = user.surname
            response['id'] = user.id
            response['token'] = token
            response['status'] = 0
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    except KeyError:
        response['error_message'] = "Invalid JSON"

    return jsonify(response)


@users.route("/<int:id>/info", methods=['GET', ])
# This part means that no permissions are checked, but a token is still required
@role_required(None)
def get_user_info(info, id):
    """
    Get all non-technical information or a particular user
    Can be used only by the staff or the user themselves
    """
    response = {'status': -1, 'error_message': ''}

    if info['role'] == -1 and info['user_id'] != id:
        response['error_message'] = 'Insufficient rights'
        return jsonify(response)

    try:
        user = Applicant.query.filter_by(id=id).first()

        if user is None:
            response['error_message'] = "No such user found"
            return jsonify(response)

        user_info = user.applicant_info
        user_education = SchoolEducation.query.filter_by(applicant_id=id).first()

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    response['first_name'] = user.name
    response['last_name'] = user.surname
    response['middle_name'] = user.middle_name
    response['email'] = user.email
    response['phone'] = user.phone
    response['citizenship'] = user.citizenship
    response['source'] = user.source

    if user_info is not None:
        birth_date = user_info.birth_date or None
        if birth_date is not None:
            birth_date = birth_date.isoformat()

        response['birth_date'] = birth_date
        response['gender'] = user_info.gender
        response['country'] = user_info.country_of_living
        response['city'] = user_info.city
    else:
        response['birth_date'] = None
        response['gender'] = None
        response['country'] = None
        response['city'] = None

    if user_education is not None:
        date_diploma = user_education.diploma_date or None
        if date_diploma is not None:
            date_diploma = date_diploma.isoformat()

        response['school_country'] = user_education.country_of_study
        response['school_city'] = user_education.city_of_study
        response['school_name'] = user_education.name_of_school
        response['graduation_year'] = user_education.grad_year
        response['grade_average'] = user_education.avg_grade or None
        response['year_russian'] = user_education.russian_test_year or None
        response['grade_russian'] = user_education.russian_test_result or None
        response['year_math'] = user_education.math_test_year or None
        response['grade_math'] = user_education.math_test_result or None
        response['year_info_physics'] = user_education.infophysics_test_year or None
        response['grade_info_physics'] = user_education.infophysics_test_result or None
        response['date_diploma'] = date_diploma
    else:
        response['school_country'] = None
        response['school_city'] = None
        response['school_name'] = None
        response['graduation_year'] = None
        response['grade_average'] = None
        response['year_russian'] = None
        response['grade_russian'] = None
        response['year_math'] = None
        response['grade_math'] = None
        response['year_info_physics'] = None
        response['grade_info_physics'] = None
        response['date_diploma'] = None

    applications = user.applications
    # CAN be a case, but currently shouldn't
    if applications is not None:
        response['applications'] = []
        for application in applications:
            response['applications'].append(application.id)
    else:
        response['applications'] = None

    response['status'] = 0

    return jsonify(response)


app.register_blueprint(users)

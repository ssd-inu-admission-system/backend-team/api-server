from backend_application import app, db
from backend_application.token import *
from backend_application.model import *

from flask import jsonify, Blueprint, request
from sqlalchemy.exc import SQLAlchemyError

tests = Blueprint('tests', __name__, url_prefix='/tests')


@tests.route('', methods=['GET', ])
@role_required([-1, 0, 1])
def tests_list(info):
    """
    Get a list of test ids and program to which each test belongs
    Can be performed only by users, superadmin or manager
    """
    response = {'status': -1, 'error_message': '', 'tests': []}

    try:
        test_list = Test.query.all()
        for test in test_list:
            programs = test.programs
            for program in programs:
                response['tests'].append({
                    'test_id': test.id,
                    'title': test.test_title,
                    'description': test.description,
                    'program_id': program.id
                })

        response['status'] = 0

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@tests.route('/new', methods=['POST', ])
@role_required([0, 1])
def tests_new(info):
    """
    Create a new test
    Can be performed only by superadmin or manager
    """
    response = {'status': -1, 'error_message': ''}
    data = request.get_json()

    if data is None:
        response['error_message'] = 'No JSON sent to /tests/new. Try checking the headers.'
        return jsonify(response)

    try:
        test_title = data['title']
        description = data['description']
        test = Test(test_title=test_title, description=description)

        questions = data.get('questions', [])
        for raw_question in questions:
            q_type = raw_question['type']

            if q_type not in range(0, 3):
                response['error_message'] = 'Invalid test type in /tests/new'
                return jsonify(response)

            question = Question(
                test=test,
                question_text=raw_question['text'],
                question_type=q_type,
                question_index=raw_question['number']
            )

            test.programs.append(Program.query.first())

            options = raw_question['options']
            for raw_option in options:
                option = QuestionOption(
                    question=question,
                    option_text=raw_option['text'],
                    is_correct=raw_option['is_correct'],
                    option_index=raw_option['number']
                )

        db.session.add(test)
        db.session.flush()

        response['test_id'] = test.id

        db.session.commit()

        response['status'] = 0

    except KeyError as e:
        response['error_message'] = f'Invalid JSON: {str(e)}'

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@tests.route('/<int:test_id>/info', methods=['GET', ])
@role_required([-1, 0, 1])
def tests_info(info, test_id):
    """
    Get information about a test (not the results)
    Can be performed only by users, superadmin or manager
    """
    response = {'status': -1, 'error_message': ''}

    try:
        test = Test.query.filter_by(id=test_id).first()
        if test is None:
            response['error_message'] = 'Test with given id not found'
            return jsonify(response)

        if info['role'] == -1:
            applicant = Applicant.query.filter_by(id=info['user_id']).first()
            if applicant is None:
                response['error_message'] = "Token outdated or internal error: " \
                                            "applicant doesn't exist in /tests/id/info"
                return jsonify(response)

            # check if user should have access to this test
            applications = applicant.applications
            has_access = False
            for application in applications:
                if application.program_id in [_.id for _ in test.programs]:
                    has_access = True
                    break

            if not has_access:
                response['error_message'] = "Required test isn't avaialable for this applicant"

        response['title'] = test.test_title
        response['description'] = test.description

        response['program_id'] = test.programs[0].id

        questions = test.questions
        response['questions'] = []

        for question in questions:
            response['questions'].append({
                'number': question.question_index,
                'type': question.question_type,
                'text': question.question_text,
            })

            response['questions'][-1]['options'] = []
            options = question.options
            for option in options:
                response['questions'][-1]['options'].append({
                    'text': option.option_text,
                    'is_correct': option.is_correct,
                    'number': option.option_index
                })

        response['status'] = 0

    except KeyError as e:
        response['error_message'] = f'Invalid JSON: {str(e)}'

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@tests.route('/<int:test_id>/delete', methods=['POST', ])
@role_required([0, 1])
def tests_delete(info, test_id):
    """
    Delete a test
    Can be performed only by superadmin or manager
    """
    response = {'status': -1, 'error_message': ''}

    try:
        test = Test.query.filter_by(id=test_id).first()
        if test is None:
            response['error_message'] = 'Test with given id not found'
            return jsonify(response)

        db.session.delete(test)
        db.session.commit()

        response['status'] = 0

    except KeyError as e:
        response['error_message'] = f'Invalid JSON: {str(e)}'

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


app.register_blueprint(tests)
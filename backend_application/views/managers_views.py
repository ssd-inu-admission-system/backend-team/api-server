from backend_application import app, db
from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify, Blueprint, request

managers = Blueprint('managers', __name__, url_prefix='/managers')


@managers.route('', methods=['GET', ])
@role_required([0, 1])
def list_managers(info):
    """
    Get a list of manager IDs
    Can be used only by superadmin and managers
    """
    response = {'status': -1, 'error_message': ''}

    try:
        managers = Staff.query.filter_by(permissions=1).all()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['staff'] = []
        for i in managers:
            manager = dict(staff_id=i.id, full_name=i.full_name)
            response['staff'].append(manager)

        response['status'] = 0

    return jsonify(response)


@managers.route("/<int:id>/info", methods=["GET"])
@role_required([0, 1])
def manager_info(info, id):
    """
    Get information about particular manager
    Can be used by superadmin and managers
    """
    response = {'status': -1, 'error_message': ''}

    try:
        manager_info = Staff.query.filter_by(permissions=1, id=id).first()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    if manager_info is None:
        response['error_message'] = "No such manager found"
        return jsonify(response)

    response['full_name'] = manager_info.full_name
    response['status'] = 0

    return jsonify(response)


@managers.route("/new", methods=["POST"])
@role_required([0, 1])
def manager_add_new(info):
    """
    Create a new manager in the system
    Can be used only by superadmin
    """
    response = {'status': -1, 'error_message': ''}

    data = request.get_json()

    if data is None:
        response['error_message'] = "No JSON sent to /managers/new. Try checking the headers."
        return jsonify(response)

    try:
        existing_manager = Staff.query.filter_by(permissions=1, email=data['email']).first()

        if existing_manager is not None:
            response['error_message'] = 'This email is already registered'
            return jsonify(response)

        # 1 - Manager permissions
        manager_new = Staff(permissions=1, full_name=data['full_name'], email=data['email'],
                            password_hash=data['password_hash'])
        db.session.add(manager_new)
        db.session.commit()

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    except KeyError:
        response['error_message'] = "Missing JSON fields"
    else:
        response['id'] = manager_new.id
        response['status'] = 0

    return jsonify(response)


@managers.route("/<int:id>/delete", methods=["GET"])
@role_required([0, 1])
def manager_delete(info, id):
    """
    Remove a manager from the system
    Can be used only by superadmin
    """
    response = {'status': -1, 'error_message': ''}

    try:
        manager_info = Staff.query.filter_by(permissions=1, id=id).first()

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    if manager_info is None:
        response['error_message'] = "No such manager found"
        return jsonify(response)

    try:
        db.session.delete(manager_info)
        db.session.commit()
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
    else:
        response['status'] = 0

    return jsonify(response)


app.register_blueprint(managers)

from backend_application import app, db
from backend_application.token import *
from backend_application.model import *

from flask import jsonify, request

from datetime import timedelta


@app.route('/staff/login', methods=['POST', ])
def staff_login():
    """
    Log into the system with the staff credentials
    Returns the access token with the id and the security permissions, along with the staff name
    """
    response = {'status': -1, 'error_message': '', 'token': ''}
    if not request.is_json:
        response['error_message'] = 'JSON expected'
        return jsonify(response)

    j = request.get_json()

    try:
        email = j['email']
        password_hash = j['password_hash']

        staff = Staff.query.filter_by(email=email, password_hash=password_hash).first()

        if not staff:
            response['error_message'] = 'User not found'
        else:
            token = generate_jwt_token(staff.id, staff.permissions, timedelta(days=60))
            response['token'] = token

            response['id'] = staff.id
            response['full_name'] = staff.full_name
            response['account_type'] = staff.permissions

            response['status'] = 0

    except KeyError:
        response['error_message'] = 'Invalid JSON'

    return jsonify(response)


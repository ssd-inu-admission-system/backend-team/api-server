from backend_application import app, db
from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify, Blueprint, request

programs = Blueprint('programs', __name__, url_prefix='/programs')


@programs.route('', methods=['GET', ])
@role_required(None)
def list_programs(info):
    """
    Get a list of programs
    """
    response = {'status': -1, 'error_message': ''}

    try:
        programmes = Program.query.all()

        response['programs'] = []
        for i in programmes:
            programme = dict(program_id=i.id, program_name=i.program_name)
            response['programs'].append(programme)

        response['status'] = 0

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@programs.route("/<int:id>/info", methods=["GET"])
@role_required(None)
def program_info(info, id):
    """
    Get information about particular program
    """
    response = {'status': -1, 'error_message': ''}

    try:
        programme_info = Program.query.filter_by(id=id).first()

        if programme_info is None:
            response['error_message'] = "No such program found"
            return jsonify(response)

        response['program_name'] = programme_info.program_name
        response['status'] = 0

        for test in programme_info.tests:
            response['tests'].append(test.id)

        for application in programme_info.applications:
            response['applications'].append(application.id)

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]
        return jsonify(response)

    return jsonify(response)


app.register_blueprint(programs)

from backend_application import app, db
from backend_application.model import *
from backend_application.token import *

from sqlalchemy.exc import SQLAlchemyError

from flask import jsonify, Blueprint, request

interviewers = Blueprint('interviewers', __name__, url_prefix='/interviewers')


@interviewers.route('', methods=['GET', ])
@role_required([0, 1])
def list_interviewers(info):
    """
    Get a list of interviewer IDs
    Can be used only by superadmin and managers
    """
    response = {'status': -1, 'error_message': '', 'staff': []}

    try:
        interviewers = Staff.query.filter_by(permissions=2).all()

        for i in interviewers:
            interviewer = dict(staff_id=i.id, full_name=i.full_name)
            response['staff'].append(interviewer)

        response['status'] = 0

    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@interviewers.route('/<int:id>/info', methods=['GET', ])
@role_required([0, 1, 2])
def get_interviewer_info(info, id):
    """
    Get information about particular interviewer
    Can be used by superadmin, managers and the interviewer himself
    """
    response = {'status': -1, 'error_message': '', 'full_name': '', 'assigned_to': []}

    if info['role'] == 2 and info['user_id'] != id:
        response['error_message'] = 'Not allowed'
        return jsonify(response)

    try:
        interviewer_info = Staff.query.filter_by(id=id, permissions=2).first()

        if not interviewer_info:
            response['error_message'] = 'No such interviewer'
            return jsonify(response)

        response['full_name'] = interviewer_info.full_name
        response['assigned_to'] = [_.id for _ in interviewer_info.applications]

        response['status'] = 0
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@interviewers.route('/<int:id>/delete', methods=['GET', ])
@role_required([0])
def interviewer_delete(info, id):
    """
    Remove an interviewer from the system
    Can be used only by superadmin
    """
    response = {'status': -1, 'error_message': ''}

    try:
        staff = Staff.query.filter_by(id=id, permissions=2).first()

        if not staff:
            response['error_message'] = 'No such interviewer'
        else:
            db.session.delete(staff)
            db.session.commit()
            response['status'] = 0
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@interviewers.route('/new', methods=['POST', ])
@role_required([0])
def interviewer_new(info):
    """
    Create a new interviewer in the system
    Can be used only by superadmin
    """
    response = {'status': -1, 'error_message': ''}

    if not request.is_json:
        response['error_message'] = 'JSON expected'
        return jsonify(response)

    j = request.get_json()

    try:
        existing_interviewer = Staff.query.filter_by(permissions=2, email=j['email']).first()
        if existing_interviewer is not None:
            response['error_message'] = 'This email is already registered'
            return jsonify(response)

        interviewer = Staff(
            permissions=2,
            full_name=j['full_name'],
            email=j['email'],
            password_hash=j['password_hash']
        )
        db.session.add(interviewer)
        db.session.commit()
        response['id'] = interviewer.id
        response['status'] = 0

    except KeyError:
        response['error_message'] = 'Invalid JSON'
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


@interviewers.route('/<int:id>/assign', methods=['POST', ])
@role_required([0, 1])
def interviewer_appoint(info, id):
    """
    Assign an interviewer for an interview for an application
    Can be used by managers and superadmin
    """
    response = {'status': -1, 'error_message': ''}

    if not request.is_json:
        response['error_message'] = 'JSON expected'
        return jsonify(response)

    j = request.get_json()

    try:
        application = Application.query.filter_by(id=j['application_id']).first()
        interviewer = Staff.query.filter_by(id=id, permissions=2).first()

        if application is None:
            response['error_message'] = 'No such application found'
            return jsonify(response)

        if interviewer is None:
            response['error_message'] = 'No such interviewer found'
            return jsonify(response)

        if application.interviewer == interviewer:
            response['error_message'] = 'Interviewer already assigned to the application'
            return jsonify(response)

        application.interviewer = interviewer
        db.session.commit()
        response['status'] = 0

    except KeyError:
        response['error_message'] = 'Invalid JSON'
    except SQLAlchemyError as e:
        response['error_message'] = e.args[0]

    return jsonify(response)


app.register_blueprint(interviewers)

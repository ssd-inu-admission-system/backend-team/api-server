import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

import logging
handler = logging.FileHandler('api-server.log')
handler.setLevel(logging.ERROR)

app = Flask(__name__)
# Get the directory about the app and set it as the root path
# Needed for file saving in tha path above, since flask (intentionally) doesn't work with relative paths
app.root_path = os.path.dirname(app.root_path)
app.logger.addHandler(handler)

app.config.from_object('backend_application.app_config.DevelopmentConfig')

# Stuff that fixes single-page application issues or something
CORS(app)

db = SQLAlchemy(app)

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = "ajlfhlakdfhsdfgj"

    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/data.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    UPLOAD_FOLDER = 'documents'
    # TODO: Maybe add a lot more extensions
    # This method is CERTAINLY not foolproof, but I guess that provides at least some security
    ALLOWED_EXTENSIONS = ["pdf", "txt", "doc", "docx"]


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://dbuser:dbpass@localhost:5432/dbuser'


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'

from backend_application.application import app, db

from flask import jsonify, request

from jwt import encode, decode, ExpiredSignatureError, InvalidIssuerError
from jwt.exceptions import DecodeError

from datetime import datetime
from functools import wraps


def generate_jwt_token(user_id, role, expiration_period):
    """Generates a JWT token for user with specified login

    user_id: the if of the staff member or the applicant which identifies them uniquely
    role: specifies access rights, similar to DB Model.
    expiration_period: datetime.timedelta specifying the period for which the token will be valid
    """
    return str(encode({
            'iss': 'SWD-App',
            'exp': datetime.utcnow() + expiration_period,
            'user_id': user_id,
            'role': role
        }, app.config['SECRET_KEY'], algorithm='HS256'), 'utf-8')


def check_jwt_token(token):
    """Checks JWT token for validity and returns its payload

    The function checks the token expiration date and issuer.

    token: base64 string in the format xxxx.yyyy.zzzz (standard JWT token representation)
    """
    try:
        info = decode(token, app.config['SECRET_KEY'], issuer='SWD-App', algorithms=('HS256',))
        return True, info
    except ExpiredSignatureError:
        return False, 'JWT token expired'
    except InvalidIssuerError:
        return False, 'Invalid issuer'
    except DecodeError:
        return False, 'Invalid token'


def role_required(role: list = None):
    """Decorator that checks if there is a valid token in arguments
    and if it is of specified staff roles, similar to DB Model,
    with only exception being that -1 is for Applicants.
    """
    def token_required(fn):
        @wraps(fn)
        def token_wrapper(*args, **kwargs):
            if 'token' not in request.args:
                return jsonify({'status': -1, 'error_message': 'Expected token in url parameters'})

            token = request.args['token']

            valid, info = check_jwt_token(token)

            if not valid:
                return jsonify({'status': -1, 'error_message': info})

            if role is not None and info['role'] not in role:
                return jsonify({'status': -1, 'error_message': 'Insufficient rights'})

            return fn(info, *args, **kwargs)

        return token_wrapper

    return token_required

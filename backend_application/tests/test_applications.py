import unittest
from datetime import date

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class ApplicationsTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(ApplicationsTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(ApplicationsTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        manager = Staff(permissions=1, full_name="Test Testov", email="test@mail.mail", password_hash="hash")

        interviewer = Staff(permissions=2, full_name="Testina Testova", email="testtoo@mail.mail",
                            password_hash="another_hash")

        user = Applicant(name="User", surname="Userov", middle_name="Userovich", citizenship="Nowheristan",
                         source="Psychic waves", phone="+99992123213", email="user@mail.mail", password_hash="userhash")
        user_info = ApplicantInfo(applicant=user, birth_date=date.today(), gender=True,
                                  country_of_living="Nowhere", city="Nowherevo")
        user_education = SchoolEducation(applicant=user, country_of_study="Here", city_of_study="This one",
                                         name_of_school="School", grad_year=100, avg_grade=999.98,
                                         russian_test_year=150, russian_test_result=10, math_test_year=160,
                                         math_test_result=15, infophysics_test_year=90, infophysics_test_result=1,
                                         diploma_date=date.today())
        program = Program(program_name="Test program")
        application = Application(applicant=user, program=program)

        with app.app_context():
            db.session.add(manager)
            db.session.add(interviewer)
            db.session.add(user)
            db.session.add(user_info)
            db.session.add(user_education)
            db.session.add(program)
            db.session.add(application)
            db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/users/login', json={
                'email': 'user@mail.mail',
                'password_hash': 'userhash'
            })

            self.user_token = token_info.get_json()["token"]

            token_info = a.post('/staff/login', json={
                'email': 'test@mail.mail',
                'password_hash': 'hash'
            })

            self.staff_token = token_info.get_json()["token"]

            token_info = a.post('/staff/login', json={
                'email': "testtoo@mail.mail",
                'password_hash': "another_hash"
            })

            self.interviewer_token = token_info.get_json()["token"]

    def tearDown(self):
        with app.app_context():
            db.session.query(Staff).delete()
            db.session.query(Applicant).delete()
            db.session.query(ApplicantInfo).delete()
            db.session.query(SchoolEducation).delete()
            db.session.query(Program).delete()
            db.session.query(Application).delete()
            db.session.commit()

    def setupTests(self, test_correctness=0):
        """
        Test info requires a bit of setting up/tearing down, but is needed only in some tests
        :param test_correctness: 0 is all correct, 1 is single choice is correct, single multiple correct,
         2 is multiple choice, 3 is wrong, 4 - no answers
        """
        test = Test(test_title="Test Test")

        question_choice = Question(test=test, question_text="2+2=?", question_type=1, question_index=1)
        question_option_right = QuestionOption(question=question_choice, option_text="4",
                                               is_correct=True, option_index=1)
        question_option_wrong = QuestionOption(question=question_choice, option_text="5",
                                               is_correct=False, option_index=2)

        question_choice_multiple = Question(test=test, question_text="x^2=4", question_type=2, question_index=2)
        question_option_multi_right = QuestionOption(question=question_choice_multiple, option_text="2",
                                                     is_correct=True, option_index=1)
        question_option_multi_right2 = QuestionOption(question=question_choice_multiple, option_text="-2",
                                                      is_correct=True, option_index=2)
        question_option_multi_wrong = QuestionOption(question=question_choice_multiple, option_text="5",
                                                     is_correct=False, option_index=3)

        if test_correctness != 4:
            test_attempt = TestAttempt(grade=50.0, application_id=1, test=test)

        # When inserting the data, all data connected by relationships is also inserted, leading to bugs
        # So, here's an ugly solution to this
        if test_correctness == 0 or test_correctness == 1:
            correct_single = QuestionChoice(test_attempt=test_attempt, question=question_choice,
                                            question_option=question_option_right, question_id=2)
        if test_correctness == 2 or test_correctness == 3:
            wrong_single = QuestionChoice(test_attempt=test_attempt, question=question_choice,
                                          question_option=question_option_wrong, question_id=2)
        if test_correctness == 0 or test_correctness == 1 or test_correctness == 2:
            correct_multi1 = QuestionChoice(test_attempt=test_attempt, question=question_choice_multiple,
                                            question_option=question_option_multi_right, question_id=3)
        if test_correctness == 0 or test_correctness == 2:
            correct_multi2 = QuestionChoice(test_attempt=test_attempt, question=question_choice_multiple,
                                            question_option=question_option_multi_right2, question_id=3)
        if test_correctness == 1 or test_correctness == 3:
            wrong_multi = QuestionChoice(test_attempt=test_attempt, question=question_choice_multiple,
                                         question_option=question_option_multi_wrong, question_id=3)

        with app.app_context():
            db.session.add(test)

            db.session.add(question_choice)
            db.session.add(question_option_right)
            db.session.add(question_option_wrong)

            db.session.add(question_choice_multiple)
            db.session.add(question_option_multi_right)
            db.session.add(question_option_multi_right2)
            db.session.add(question_option_multi_wrong)

            if test_correctness != 4:
                db.session.add(test_attempt)

            if test_correctness == 0:
                db.session.add(correct_single)
                db.session.add(correct_multi1)
                db.session.add(correct_multi2)
            elif test_correctness == 1:
                db.session.add(correct_single)
                db.session.add(correct_multi1)
                db.session.add(wrong_multi)
            elif test_correctness == 2:
                db.session.add(wrong_single)
                db.session.add(correct_multi1)
                db.session.add(correct_multi2)
            elif test_correctness == 3:
                db.session.add(wrong_single)
                db.session.add(wrong_multi)

            db.session.commit()

    def teardownTests(self):
        with app.app_context():
            db.session.query(Test).delete()
            db.session.query(Question).delete()
            db.session.query(TestAttempt).delete()
            db.session.query(QuestionChoice).delete()
            db.session.query(QuestionOption).delete()
            db.session.commit()

    def test1_list(self):
        """
        Test function that gets list of applications
        """
        with app.test_client() as a:
            res = a.get('/applications', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application_data = data["applications"]

            assert len(application_data) == 1

            application = application_data[0]

            assert application is not None
            assert application['application_id'] == 1
            assert application['applicant_id'] == 1
            assert application['applicant_name'] == "User"
            assert application['applicant_surname'] == "Userov"
            assert application['application_status'] == 0

    def test2_info(self):
        """
        Test function that gets info about a particular test
        """
        self.setupTests()

        with app.test_client() as a:
            res = a.get('/applications/1/info', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data['applicant_id'] == 1
            assert data['program_id'] == 1
            assert data['interviewer_id'] is None
            assert data['grade'] is None
            assert data['comment'] is None

            tests = data['tests_info']

            assert len(tests) == 1
            assert tests[0]['test_id'] == 1
            assert tests[0]['correct'] == 2
            assert tests[0]['total'] == 2
            assert tests[0]['grade'] == 50.0

            documents = data['documents']
            assert len(documents) == 0

        self.teardownTests()

        self.setupTests(test_correctness=1)

        with app.test_client() as a:
            res = a.get('/applications/1/info', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data['applicant_id'] == 1
            assert data['program_id'] == 1

            tests = data['tests_info']

            assert len(tests) == 1
            assert tests[0]['test_id'] == 1
            assert tests[0]['correct'] == 1
            assert tests[0]['total'] == 2
            assert tests[0]['grade'] == 50.0

            documents = data['documents']
            assert len(documents) == 0

        self.teardownTests()

        self.setupTests(test_correctness=2)

        with app.test_client() as a:
            res = a.get('/applications/1/info', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data['applicant_id'] == 1
            assert data['program_id'] == 1

            tests = data['tests_info']

            assert len(tests) == 1
            assert tests[0]['test_id'] == 1
            assert tests[0]['correct'] == 1
            assert tests[0]['total'] == 2
            assert tests[0]['grade'] == 50.0

            documents = data['documents']
            assert len(documents) == 0

        self.teardownTests()

        self.setupTests(test_correctness=3)

        with app.test_client() as a:
            res = a.get('/applications/1/info', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data['applicant_id'] == 1
            assert data['program_id'] == 1

            tests = data['tests_info']

            assert len(tests) == 1
            assert tests[0]['test_id'] == 1
            assert tests[0]['correct'] == 0
            assert tests[0]['total'] == 2
            assert tests[0]['grade'] == 50.0

            documents = data['documents']
            assert len(documents) == 0

        self.teardownTests()

    def test3_create(self):
        """
        Test function that creates a new test
        """
        with app.test_client() as a:
            # Clear the existing applications
            Application.query.filter_by(applicant_id=1).delete()
            db.session.commit()

            res = a.post('/applications/create', query_string=dict(token=self.user_token), json={"applicant_id": 1})

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application = Application.query.filter_by(applicant_id=1).all()

            assert application is not None
            assert len(application) == 1

            application = application[0]
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.status == 0

    def test4_tests_results(self):
        """
        Test function that gets test results
        """
        self.setupTests(test_correctness=1)

        with app.test_client() as a:
            res = a.get('/applications/1/test_results', query_string=dict(token=self.staff_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            results = data['results']

            # TODO: Potentially incorrect indices
            assert len(results) == 1
            assert results[0]['test_id'] == 1
            assert results[0]['grade'] == 50.0
            assert len(results[0]['answers']) == 2
            assert len(results[0]['answers'][0]['answers']) == 1
            assert results[0]['answers'][0]['answers'][0] == 1
            assert len(results[0]['answers'][1]['answers']) == 2
            assert results[0]['answers'][1]['answers'][0] == 1
            assert results[0]['answers'][1]['answers'][1] == 3

        self.teardownTests()

    def test5_change_status(self):
        """
        Test function that changes the status of an application
        """
        with app.test_client() as a:
            res = a.post('/applications/1/change_status', query_string=dict(token=self.staff_token), json={"status": 2})

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.status == 2

    def test6_change_status_restrictions(self):
        """
        Test that staff cannot set incorrect status for an application
        """
        with app.test_client() as a:
            # Forbid incorrect values for status
            res = a.post('/applications/1/change_status', query_string=dict(token=self.staff_token),
                         json={"status": 42})

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.status == 0

            res = a.post('/applications/1/change_status', query_string=dict(token=self.staff_token),
                         json={"status": -100})

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.status == 0

    def test7_add_grade(self):
        """
        Test function that adds a grade to an application
        """
        with app.test_client() as a:
            res = a.post('/applications/1/grade', query_string=dict(token=self.interviewer_token), json={"grade": 90})

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.application_grade == 90

    def test8_add_comment(self):
        """
        Test function that adds a comment to an application
        """
        with app.test_client() as a:
            res = a.post('/applications/1/comment', query_string=dict(token=self.interviewer_token),
                         json={"comment": "good"})

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.application_review == "good"

    def test9_grade_restrictions(self):
        """
        Test that an interviewer cannot set incorrect grade for an application
        """
        with app.test_client() as a:
            # Forbid incorrect values for status
            res = a.post('/applications/1/grade', query_string=dict(token=self.interviewer_token),
                         json={"grade": "good grade"})

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()

            assert application is not None
            assert application.id == 1
            assert application.program_id == 1
            assert application.applicant_id == 1
            assert application.application_grade is None

    def test10_send_test(self):
        """
        Test function that sends test results
        """
        self.setupTests(test_correctness=4)

        with app.test_client() as a:
            res = a.post('/applications/1/send_test', query_string=dict(token=self.user_token),
                         json={'test_id': 1, "answers": [{'number': 1, 'answers': [1]},
                                                         {'number': 2, 'answers': [1, 3]}]})

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            attempt = TestAttempt.query.filter_by(application_id=1, test_id=1).first()

            assert attempt is not None
            assert (attempt.grade - float(1 / 3 * 100) < 0.1)

            assert attempt.question_choices is not None
            assert len(attempt.question_choices) == 3
            assert attempt.question_choices[0].question_id == 1
            assert attempt.question_choices[0].question_option_id == 1

            assert attempt.question_choices[1].question_id == 2
            assert attempt.question_choices[1].question_option_id == 3

            assert attempt.question_choices[2].question_id == 2
            assert attempt.question_choices[2].question_option_id == 5

        self.teardownTests()

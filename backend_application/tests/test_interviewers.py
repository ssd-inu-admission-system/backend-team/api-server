import unittest
from datetime import date

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class InterviewersTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(InterviewersTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(InterviewersTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        interviewer1 = Staff(permissions=2, full_name="Test Testov", email="test@mail.mail", password_hash="hash")
        interviewer2 = Staff(permissions=2, full_name="Testina Testova", email="testtoo@mail.mail",
                             password_hash="another_hash")
        manager = Staff(permissions=1, full_name="Wrong Wrongovich", email="wrong@mail.mail",
                        password_hash="hash_again")
        superadmin = Staff(permissions=0, full_name="Admin Adminov", email="admin@mail.mail",
                           password_hash="hash_admin")

        user = Applicant(name="User", surname="Userov", middle_name="Userovich", citizenship="Nowheristan",
                         source="Psychic waves", phone="+99992123213", email="user@mail.mail", password_hash="userhash")
        user_info = ApplicantInfo(applicant=user, birth_date=date.today(), gender=True,
                                  country_of_living="Nowhere", city="Nowherevo")
        user_education = SchoolEducation(applicant=user, country_of_study="Here", city_of_study="This one",
                                         name_of_school="School", grad_year=100, avg_grade=999.98,
                                         russian_test_year=150, russian_test_result=10, math_test_year=160,
                                         math_test_result=15, infophysics_test_year=90, infophysics_test_result=1,
                                         diploma_date=date.today())

        program = Program(program_name="Test program")
        application = Application(applicant=user, program=program)

        db.session.add(interviewer1)
        db.session.add(interviewer2)
        db.session.add(manager)
        db.session.add(superadmin)
        db.session.add(user)
        db.session.add(user_info)
        db.session.add(user_education)
        db.session.add(program)
        db.session.add(application)
        db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'admin@mail.mail',
                'password_hash': 'hash_admin'
            })

            self.token = token_info.get_json()["token"]

    def tearDown(self):
        db.session.query(Staff).delete()
        db.session.query(Applicant).delete()
        db.session.query(ApplicantInfo).delete()
        db.session.query(SchoolEducation).delete()
        db.session.query(Program).delete()
        db.session.query(Application).delete()
        db.session.commit()

    def test1_list(self):
        """
        Test function that gets interviewers list
        """
        with app.test_client() as a:
            res = a.get("/interviewers", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            staff_data = data['staff']

            for i in staff_data:
                if i['staff_id'] == 1:
                    assert i['full_name'] == "Test Testov"
                elif i['staff_id'] == 2:
                    assert i['full_name'] == "Testina Testova"
                else:
                    # There should be only 2 interviewers, no more
                    assert False

    def test2_info(self):
        """
        Test function that gets information about particular interviewer
        """
        with app.test_client() as a:
            res = a.get("/interviewers/1/info", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data["full_name"] == "Test Testov"
            # assert data["assigned_to"] == ""

    def test3_info_restrictions(self):
        """
        Test that no other interviewer can get information about other interviewers except himself
        """
        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'testtoo@mail.mail',
                'password_hash': 'another_hash'
            })

            token = token_info.get_json()["token"]
            res = a.get("/interviewers/1/info", query_string=dict(token=token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

    def test4_new_interviewer(self):
        """
        Test new interviewer creation
        """
        with app.test_client() as a:
            res = a.post("/interviewers/new", json={
                "full_name": "New New",
                "email": "new@mail.mail",
                "password_hash": "newhash"
            }, query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0
            assert data["id"] == 5

            interviewer = Staff.query.filter_by(id=5).first()

            assert interviewer.full_name == "New New"
            assert interviewer.email == "new@mail.mail"
            assert interviewer.password_hash == "newhash"

    def test5_new_interviewer_restriction(self):
        """
        Test that an interviewer is not created when not full information is provided
        """
        with app.test_client() as a:
            res = a.post("/interviewers/new", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            interviewer = Staff.query.filter_by(id=5).first()

            assert interviewer is None

            res = a.post("/interviewers/new", json={
                "full_name": "New New",
                "password_hash": "newhash"
            }, query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            interviewer = Staff.query.filter_by(id=5).first()

            assert interviewer is None

            # duplicate email
            res = a.post('/interviewers/new', json={
                'full_name': 'Another One',
                'email': 'test@mail.mail',
                'password_hash': 'bitesthedust'
            }, query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            interviewer = Staff.query.filter_by(id=5).first()
            assert interviewer is None

    def test6_delete(self):
        """
        Test deletion of an interviewer
        """
        with app.test_client() as a:
            res = a.get("/interviewers/2/delete", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            interviewer = Staff.query.filter_by(id=2).first()

            assert interviewer is None

    def test7_delete_restrictions(self):
        """
        Test that managers cannot delete an interviewer
        """
        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'wrong@mail.mail',
                'password_hash': 'hash_again'
            })

            token = token_info.get_json()["token"]

            res = a.get("/interviewers/1/delete", query_string=dict(token=token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            interviewer = Staff.query.filter_by(id=1).first()

            assert interviewer.full_name == "Test Testov"
            assert interviewer.email == "test@mail.mail"
            assert interviewer.password_hash == "hash"

    def test8_assign_interviewer(self):
        """
        Test interviewer assignment
        """
        with app.test_client() as a:
            res = a.post('/interviewers/1/assign', query_string=dict(token=self.token), json={
                'application_id': 1
            })

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            application = Application.query.filter_by(id=1).first()
            interviewer = Staff.query.filter_by(id=1, permissions=2).first()

            assert len(interviewer.applications) == 1
            assert application.interviewer == interviewer
            assert application in interviewer.applications

    def test9_assign_interviewer_repetition(self):
        """
        Test that we can't assign an interview twice
        """
        with app.test_client() as a:
            a.post('/interviewers/1/assign', query_string=dict(token=self.token), json={
                'application_id': 1
            })

            res = a.post('/interviewers/1/assign', query_string=dict(token=self.token), json={
                'application_id': 1
            })
            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()
            interviewer = Staff.query.filter_by(id=1, permissions=2).first()

            assert len(interviewer.applications) == 1
            assert application.interviewer == interviewer
            assert application in interviewer.applications

    def test10_assign_interviewer_exist(self):
        """
        Test that we can only assign existing applications to existing interviewers
        """
        with app.test_client() as a:
            # Incorrect interviewer
            res = a.post('/interviewers/5/assign', query_string=dict(token=self.token), json={
                'application_id': 1
            })
            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()
            interviewer = Staff.query.filter_by(id=1, permissions=2).first()

            assert len(interviewer.applications) == 0
            assert application.interviewer is None
            assert application not in interviewer.applications

            # Incorrect application
            res = a.post('/interviewers/1/assign', query_string=dict(token=self.token), json={
                'application_id': 5
            })
            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            application = Application.query.filter_by(id=1).first()
            interviewer = Staff.query.filter_by(id=1, permissions=2).first()

            assert len(interviewer.applications) == 0
            assert application.interviewer is None
            assert application not in interviewer.applications


if __name__ == "__main__":
    unittest.main()

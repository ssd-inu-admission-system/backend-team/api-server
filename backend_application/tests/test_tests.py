import os
import unittest
import datetime

from flask import Flask

from backend_application import *
from backend_application.model import *


class TestsTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestsTest, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)
        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(TestsTest, cls).tearDownClass()
        with app.app_context():
            db.drop_all()

    def setUp(self):
        manager = Staff(permissions=1, full_name='Test Testov', email='test@mail.mail', password_hash='hash')
        program = Program(program_name="Test program")

        with app.app_context():
            db.session.add(manager)
            db.session.add(program)
            db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'test@mail.mail',
                'password_hash': 'hash'
            })

            self.token = token_info.get_json()['token']

    def tearDown(self):
        with app.app_context():
            db.session.query(Staff).delete()
            # db.session.query(Test).delete()
            # doesn't work well with cascades
            for test in Test.query.all():
                db.session.delete(test)

            db.session.commit()

    def test1_add_check(self):
        """
        Test addition of a test
        """
        with app.test_client() as a:
            # add a test
            res = a.post(
                '/tests/new', json={
                    'title': 'Test Test',
                    'description': 'Test for Test Test',
                    'questions': [
                        {
                            'number': 1,
                            'type': 1,
                            'text': "2+2=?",
                            'options': [{'text': '4', 'is_correct': True, 'number': 1},
                                        {'text': '3', 'is_correct': False, 'number': 2}]
                        }
                    ]
                }, query_string=dict(token=self.token)
            )

            assert res.is_json

            data = res.get_json()

            assert data['status'] == 0
            assert len(data['error_message']) == 0

            test_id = data['test_id']

            test = Test.query.filter_by(id=test_id).first()

            assert test is not None

            questions = test.questions
            assert questions is not None
            assert len(questions) == 1

            for question in questions:
                    options = question.options
                    assert options is not None
                    assert len(options) == 2

            # list tests
            res = a.get('/tests', query_string=dict(token=self.token))
            assert res.is_json

            data = res.get_json()

            assert data['status'] == 0
            assert len(data['error_message']) == 0

            assert len(data['tests']) == 1
            assert data['tests'][0]['test_id'] == test_id

            # check test info

            res = a.get(f'/tests/{test_id}/info', query_string=dict(token=self.token))
            assert res.is_json

            data = res.get_json()

            assert data['status'] == 0
            assert len(data['error_message']) == 0

            assert data['title'] == 'Test Test'
            questions = data['questions']

            assert questions[0]['number'] == 1
            assert questions[0]['type'] == 1
            assert questions[0]['text'] == "2+2=?"

            options = questions[0]['options']

            assert options[0]['text'] == '4'
            assert options[0]['is_correct'] is True
            assert options[0]['number'] == 1
            assert options[1]['text'] == '3'
            assert options[1]['is_correct'] is False
            assert options[1]['number'] == 2

    def test2_deletion(self):
        """
        Test deletion of a test
        """
        with app.test_client() as a:
            # add a tests
            res = a.post('/tests/new', json={
                'title': 'Test Test 1',
                'description': 'Test for Test Test Test',
                'questions': [{'number': 1, 'type': 1, 'text': "3+1=?",
                               'options': [
                                   {'text': '4', 'is_correct': True, 'number': 1},
                                   {'text': '3', 'is_correct': False, 'number': 2}
                                ]}]}, query_string=dict(token=self.token))

            assert res.is_json
            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            res = a.post('/tests/new', json={
                'title': 'Test Test 2',
                'description': 'Test 2 for Test Test Test Test',
                'questions': [{'number': 1, 'type': 1, 'text': "2+2=?",
                               'options': [
                                   {'text': '4', 'is_correct': True, 'number': 1},
                                   {'text': '3', 'is_correct': False, 'number': 2}
                                ]}]}, query_string=dict(token=self.token))

            assert res.is_json
            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            test_id = data['test_id']

            tests = Test.query.all()
            assert len(tests) == 2

            questions = Question.query.all()
            assert len(questions) == 2

            question_options = QuestionOption.query.all()
            assert len(question_options) == 4

            res = a.post(f'/tests/{test_id}/delete', query_string=dict(token=self.token))
            assert res.is_json
            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            tests = Test.query.all()
            assert len(tests) == 1

            questions = Question.query.all()
            assert len(questions) == 1

            question_options = QuestionOption.query.all()
            assert len(question_options) == 2

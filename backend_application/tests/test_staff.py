import os
import unittest
import datetime

from flask import Flask

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class StaffTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(StaffTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)
        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(StaffTests, cls).tearDownClass()
        with app.app_context():
            db.drop_all()

    def setUp(self):
        manager = Staff(permissions=1, full_name="Manag Test", email="manag@inu.org", password_hash="1234")

        db.session.add(manager)
        db.session.commit()

    def tearDown(self):
        db.session.query(Staff).delete()
        db.session.commit()

    def test1_staff_login(self):
        """Login into app as a Manager
        """
        # attempt to login
        with app.test_client() as c:
            rv = c.post('/staff/login', json={
                'email': 'manag@inu.org',
                'password_hash': '1234'
            })

            # check if response is JSON
            assert rv.is_json

            j = rv.get_json()
            success, info = check_jwt_token(j['token'])
            assert success

            self.token = j['token']
            assert info['user_id'] == 1
            assert info['role'] == 1

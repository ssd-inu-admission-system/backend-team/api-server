import io
import unittest
import shutil
from datetime import date

from backend_application import *
from backend_application.model import *
from backend_application.token import *


# TODO: DO NOT RUN THE TEST WITH STUFF IN PRODUCTION!!
# I can't figure out how to change the root path of the testing client
class DocumentsTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(DocumentsTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        user = Applicant(name="User", surname="Userov", middle_name="Userovich", citizenship="Nowheristan",
                         source="Psychic waves", phone="+99992123213", email="user@mail.mail", password_hash="userhash")

        user_info = ApplicantInfo(applicant=user, birth_date=date.today(), gender=True,
                                  country_of_living="Nowhere", city="Nowherevo")

        user_education = SchoolEducation(applicant=user, country_of_study="Here", city_of_study="This one",
                                         name_of_school="School", grad_year=100, avg_grade=999.98,
                                         russian_test_year=150, russian_test_result=10, math_test_year=160,
                                         math_test_result=15, infophysics_test_year=90, infophysics_test_result=1,
                                         diploma_date=date.today())

        program = Program(program_name="Test program")
        application = Application(applicant=user, program=program)

        with app.app_context():
            db.drop_all()
            db.create_all()

            db.session.add(user)
            db.session.add(user_info)
            db.session.add(user_education)
            db.session.add(program)
            db.session.add(application)
            db.session.commit()

    @classmethod
    def tearDownClass(cls):
        super(DocumentsTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        with app.test_client() as a:
            token_info = a.post('/users/login', json={
                'email': 'user@mail.mail',
                'password_hash': 'userhash'
            })

            self.user_token = token_info.get_json()["token"]

            self.files_path = f"{app.root_path}/{app.config['UPLOAD_FOLDER']}"

    def tearDown(self):
        with app.test_client() as a:
            db.session.query(Document).delete()
            db.session.commit()

            if os.path.exists(self.files_path):
                shutil.rmtree(self.files_path)

    def test1_upload(self):
        """
        Test function that uploads a document
        :return:
        """
        with app.test_client() as a:
            res = a.post("/documents/1/send",
                         data={
                             "type": "1",
                             "document": (io.BytesIO(b"test text testing the testers"), "test.txt")
                         },
                         query_string=dict(token=self.user_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0
            assert data['document_id'] == 1

            res = a.post("/documents/1/send",
                         data={
                             "type": "2",
                             "document": (
                                 io.BytesIO(b"test text testing the tired testers twice together"), "test2.txt")
                         },
                         query_string=dict(token=self.user_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0
            assert data['document_id'] == 2

            assert os.path.isdir(self.files_path)
            assert os.path.isdir(self.files_path + "/1")
            assert os.path.isdir(self.files_path + "/1/1")
            assert os.path.exists(self.files_path + "/1/1/test.txt")
            assert os.path.isdir(self.files_path)
            assert os.path.isdir(self.files_path + "/1")
            assert os.path.isdir(self.files_path + "/1/2")
            assert os.path.exists(self.files_path + "/1/2/test2.txt")

            documents = Document.query.all()

            assert len(documents) == 2
            assert documents[0].id == 1
            assert documents[0].application_id == 1
            assert documents[1].id == 2
            assert documents[1].application_id == 1

    def test2_upload_restrictions(self):
        with app.test_client() as a:
            """
            Test that the server accepts documents with supported extensions
            """
            # Only accept certain document extensions
            res = a.post("/documents/1/send",
                         data={
                             "document": (io.BytesIO(b"BAD EVIL SHELL CODE 'ERE"), "test.sh")
                         },
                         query_string=dict(token=self.user_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            assert not os.path.isdir(self.files_path)

            documents = Document.query.all()

            assert len(documents) == 0

            """
            Test that the files of the same type replace the previous ones
            """
            a.post("/documents/1/send",
                   data={
                       "type": "1",
                       "document": (io.BytesIO(b"test text testing the testers"), "test.txt")
                   },
                   query_string=dict(token=self.user_token))

            res = a.post("/documents/1/send",
                         data={
                             "type": "1",
                             "document": (
                                 io.BytesIO(b"test text testing the tired testers twice together"), "test2.txt")
                         },
                         query_string=dict(token=self.user_token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert os.path.isdir(self.files_path)
            assert os.path.isdir(self.files_path + "/1")
            assert os.path.isdir(self.files_path + "/1/1")
            assert os.path.exists(self.files_path + "/1/1/test2.txt")
            assert not os.path.exists(self.files_path + "/1/1/test.txt")

            documents = Document.query.all()

            assert len(documents) == 1
            assert documents[0].id == 1
            assert documents[0].application_id == 1
            assert documents[0].document_type == 1

    def test3_get_file(self):
        """
        Test that the server receives the document that was sent
        """
        with app.test_client() as a:
            a.post("/documents/1/send",
                   data={
                       "type": 1,
                       "document": (io.BytesIO(b"test text testing the testers"), "test.txt")
                   },
                   query_string=dict(token=self.user_token))

            res = a.get("/documents/1/1", query_string=dict(token=self.user_token))

            assert res.status_code == 200
            assert res.data == b"test text testing the testers"

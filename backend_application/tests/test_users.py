import unittest
from datetime import date

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class UsersTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(UsersTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(UsersTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        user = Applicant(name="User", surname="Userov", middle_name="Userovich", citizenship="Nowheristan",
                         source="Psychic waves", phone="+99992123213", email="user@mail.mail", password_hash="userhash")
        user_info = ApplicantInfo(applicant=user, birth_date=date.today(), gender=True,
                                  country_of_living="Nowhere", city="Nowherevo")
        user_education = SchoolEducation(applicant=user, country_of_study="Here", city_of_study="This one",
                                         name_of_school="School", grad_year=100, avg_grade=999.98,
                                         russian_test_year=150, russian_test_result=10, math_test_year=160,
                                         math_test_result=15, infophysics_test_year=90, infophysics_test_result=1,
                                         diploma_date=date.today())

        manager = Staff(permissions=1, full_name="Test Testov", email="test@mail.mail", password_hash="hash")
        interviewer = Staff(permissions=2, full_name="Wrong Wrongovich", email="wrong@mail.mail",
                            password_hash="hash_again")
        program = Program(program_name="Test program")
        application = Application(applicant=user, program=program)

        with app.app_context():
            db.session.add(user)
            db.session.add(user_info)
            db.session.add(user_education)
            db.session.add(manager)
            db.session.add(interviewer)
            db.session.add(program)
            db.session.add(application)
            db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/users/login', json={
                'email': 'user@mail.mail',
                'password_hash': 'userhash'
            })

            self.user_token = token_info.get_json()["token"]

            token_info = a.post('/staff/login', json={
                'email': 'test@mail.mail',
                'password_hash': 'hash'
            })

            self.staff_token = token_info.get_json()["token"]

    def tearDown(self):
        with app.app_context():
            db.session.query(Applicant).delete()
            db.session.query(ApplicantInfo).delete()
            db.session.query(SchoolEducation).delete()
            db.session.query(Staff).delete()
            db.session.query(Program).delete()
            db.session.query(Application).delete()
            db.session.commit()

    def test1_sign_up(self):
        """
        Test function that performs sign up for users
        """
        with app.test_client() as a:
            res = a.post('/users/sign_up', json={
                'first_name': "Applicant", 'last_name': "Appu",
                'citizenship': "Nowheristan",
                'source': "Demons in my head", 'phone': "+99992123213", 'email': "usertoo@mail.mail",
                'password_hash': "userhash2"
            })

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            user = Applicant.query.filter_by(id=2).first()

            assert user is not None
            assert user.name == "Applicant"
            assert user.surname == "Appu"
            assert user.middle_name is None
            assert user.citizenship == "Nowheristan"
            assert user.source == "Demons in my head"
            assert user.phone == "+99992123213"
            assert user.email == "usertoo@mail.mail"
            assert user.password_hash == "userhash2"

    def test2_sign_up_restrictions(self):
        """
        Test that a user cannot sign up without a name
        """
        with app.test_client() as a:
            # A user with no name
            res = a.post('/users/sign_up', json={
                'last_name': "Appu", 'citizenship': "Nowheristan",
                'source': "Demons in my head", 'phone': "+99992123213",
                'email': "usertoo@mail.mail", 'password_hash': "userhash2"
            })

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] < 0
            assert len(data['error_message']) != 0

            user = Applicant.query.filter_by(id=2).first()

            assert user is None

    def test3_login(self):
        """
        Test function that performs sign in for users
        """
        with app.test_client() as a:
            res = a.post('/users/login', json={
                'email': 'user@mail.mail',
                'password_hash': 'userhash'
            })

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            assert data.get('token', None) is not None

            success, info = check_jwt_token(data['token'])
            assert success
            assert info['role'] == -1
            assert info['user_id'] == 1

    def test4_login_restrictions(self):
        """
        Test that a user cannot login with wrong password hash
        """
        with app.test_client() as a:
            res = a.post('/users/login', json={
                'email': 'userrrrr@mail.mail',
                'password_hash': 'userhaaaash'
            })

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] < 0
            assert len(data['error_message']) != 0

            assert data.get('token', None) is None

    def test5_list(self):
        """
        Test function that gets a list of users when requested by staff
        """
        with app.test_client() as a:
            res = a.get('/users', query_string=dict(token=self.staff_token))

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            users = data['users']

            assert len(users) == 1
            assert users[0]['id'] == 1
            assert users[0]['first_name'] == "User"
            assert users[0]['last_name'] == "Userov"
            assert users[0]['middle_name'] == "Userovich"

    def test6_list_restriction(self):
        """
        Test that a user cannot get a list of other users
        """
        with app.test_client() as a:
            res = a.get('/users', query_string=dict(token=self.user_token))

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] < 0
            assert len(data['error_message']) != 0

            assert data.get('users', None) is None

    def test7_info(self):
        """
        Test function that gets info about a particular user when staff requests
        """
        with app.test_client() as a:
            res = a.get('/users/1/info', query_string=dict(token=self.staff_token))

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            assert data['first_name'] == "User"
            assert data['last_name'] == "Userov"
            assert data['middle_name'] == "Userovich"
            assert data['birth_date'] == date.today().isoformat()
            assert data['gender'] is True
            assert data['citizenship'] == "Nowheristan"
            assert data['country'] == "Nowhere"
            assert data['city'] == "Nowherevo"
            assert data['source'] == "Psychic waves"
            assert data['phone'] == "+99992123213"
            assert data['email'] == "user@mail.mail"
            assert data['school_country'] == "Here"
            assert data['school_city'] == "This one"
            assert data['school_name'] == "School"
            assert data['graduation_year'] == 100
            assert data['grade_average'] == 999.98
            assert data['year_russian'] == 150
            assert data['grade_russian'] == 10
            assert data['year_math'] == 160
            assert data['grade_math'] == 15
            assert data['year_info_physics'] == 90
            assert data['grade_info_physics'] == 1
            assert data['date_diploma'] == date.today().isoformat()
            assert len(data['applications']) == 1
            assert 1 in data['applications']

    def test8_info_personal(self):
        """
        Test that a user can get information about himself
        """
        with app.test_client() as a:
            res = a.get('/users/1/info', query_string=dict(token=self.user_token))

            assert res is not None
            assert res.is_json

            data = res.get_json()
            assert data['status'] == 0
            assert len(data['error_message']) == 0

            assert data['first_name'] == "User"
            assert data['last_name'] == "Userov"
            assert data['middle_name'] == "Userovich"
            assert data['birth_date'] == date.today().isoformat()
            assert data['gender'] is True
            assert data['citizenship'] == "Nowheristan"
            assert data['country'] == "Nowhere"
            assert data['city'] == "Nowherevo"
            assert data['source'] == "Psychic waves"
            assert data['phone'] == "+99992123213"
            assert data['email'] == "user@mail.mail"
            assert data['school_country'] == "Here"
            assert data['school_city'] == "This one"
            assert data['school_name'] == "School"
            assert data['graduation_year'] == 100
            assert data['grade_average'] == 999.98
            assert data['year_russian'] == 150
            assert data['grade_russian'] == 10
            assert data['year_math'] == 160
            assert data['grade_math'] == 15
            assert data['year_info_physics'] == 90
            assert data['grade_info_physics'] == 1
            assert data['date_diploma'] == date.today().isoformat()
            assert len(data['applications']) == 1
            assert 1 in data['applications']

# this is not a real test and shouldn't really be added to __init__.py

from backend_application import app, db
from backend_application.model import *
from datetime import date
import json


def fill_mock_data():

    # Super Administrator

    db.session.add(Staff(permissions=0, full_name="Gordon Freeman", email="admin@admin.ru", password_hash="admin"))
    db.session.commit()

    # Program

    program = Program(program_name="Test program")
    db.session.add(program)
    db.session.commit()

    # Staff

    with open('backend_application/tests/mocs/staff.json') as mock_file:
        mock_data = json.load(mock_file)
        for row in mock_data:
            db.session.add(Staff(**row))

        db.session.commit()

    # Applicants and Applications

    with open('backend_application/tests/mocs/applicant.json') as a_mock_file, \
            open('backend_application/tests/mocs/applicant_info.json') as ai_mock_file:
        a_mock_data = json.load(a_mock_file)
        ai_mock_data = json.load(ai_mock_file)
        for a_row, ai_row in zip(a_mock_data, ai_mock_data):
            ai_row['birth_date'] = date.fromisoformat(ai_row['birth_date'])
            a = Applicant(**a_row)
            ai = ApplicantInfo(applicant=a, **ai_row)
            # applications
            ap = Application(applicant=a, program=program)
            # should also add applicant
            db.session.add(ai)

        db.session.commit()

    applicant = Applicant.query.first()

    # test

    test = Test(test_title="Test Test 1")
    program.tests.append(test)

    question2 = Question(question_text="Single-Choice Question 1", question_type=1, question_index=1)

    option1 = QuestionOption(option_text="Single-Choice Question 1 Option 1", is_correct=True, option_index=1)
    option2 = QuestionOption(option_text="Single-Choice Question 1 Option 2", is_correct=False, option_index=2)

    question2.options.extend([option1, option2])
    test.questions.append(question2)

    # test attempt

    test_attempt = TestAttempt(application=Application.query.first(), test=test)

    a2 = QuestionChoice(
        test_attempt=test_attempt,
        question=question2,
        question_option=option1
    )

    db.session.add(a2)

    db.session.commit()



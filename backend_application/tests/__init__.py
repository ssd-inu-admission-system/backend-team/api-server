from backend_application.tests.test_database import *
from backend_application.tests.test_staff import *
from backend_application.tests.test_managers import *
from backend_application.tests.test_interviewers import *
from backend_application.tests.test_programs import *
from backend_application.tests.test_tests import *
from backend_application.tests.test_applications import *
from backend_application.tests.test_documents import *

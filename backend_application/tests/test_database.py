import os
import unittest
import datetime

from flask import Flask

from backend_application import *
from backend_application.model import *


class DataBaseTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(DataBaseTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)
        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(DataBaseTests, cls).tearDownClass()
        with app.app_context():
            db.drop_all()

    def test1_populate_db(self):
        """
        Populate DB with Program, Test, and 2 Questions
        """
        with app.app_context():
            prog = Program(program_name="Test Program 1")
            db.session.add(prog)

            test = Test(test_title="Test Test 1")
            prog.tests.append(test)

            question1 = Question(question_text="Single-Choice Question 1", question_type=1, question_index=1)

            option1 = QuestionOption(option_text="Single-Choice Question 1 Option 1", is_correct=True, option_index=1)
            option2 = QuestionOption(option_text="Single-Choice Question 1 Option 2", is_correct=False, option_index=2)

            question1.options.extend([option1, option2])

            test.questions.append(question1)

            db.session.commit()

    def test2_add_applicant(self):
        """
        Add applicant, application, and question answers
        """
        with app.app_context():
            prog = Program.query.first()

            applicant = Applicant(
                name="TName",
                surname="TSurname",

                citizenship="Martian",
                source="Propaganda",

                phone="88005553535",
                email="test@test.test",
                password_hash="testtest"
            )

            applicant_info = ApplicantInfo(
                applicant=applicant,

                birth_date=datetime.datetime.strptime('2001-01-01', '%Y-%m-%d'),
                gender=True,
                country_of_living="Armageddon",
                city="Kadia"
            )

            education_info = SchoolEducation(
                applicant=applicant,

                country_of_study="Terra",
                city_of_study="Moscow",
                name_of_school="Emperor's School of Primarchs",
                grad_year="3075",
                avg_grade="18"
            )

            db.session.add(education_info)

            applicant.education_type = 0
            applicant.education_id = education_info.id

            application = Application(applicant=applicant, program=prog)
            db.session.add(applicant)

            q1 = Question.query.filter_by(question_type=1).first()
            test_attempt = TestAttempt(application=application, test=q1.test)

            q1_options = QuestionOption.query.filter_by(question=q1).all()
            a2 = QuestionChoice(
                test_attempt=test_attempt,
                question=q1,
                question_option=q1_options[1]
            )

            db.session.add(a2)

            db.session.commit()

    def test3_add_staff(self):
        """
        Populate DB with some Staff
        TODO: add more staff, assign application to interviewer, whatever.
        """
        with app.app_context():
            manager = Staff(permissions=1, full_name="Manag Test", email="manag@inu.org", password_hash="1234")
            db.session.add(manager)
            db.session.commit()

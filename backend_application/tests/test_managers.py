import unittest

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class ManagersTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(ManagersTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(ManagersTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        manager1 = Staff(permissions=1, full_name="Test Testov", email="test@mail.mail", password_hash="hash")
        manager2 = Staff(permissions=1, full_name="Testina Testova", email="testtoo@mail.mail",
                         password_hash="another_hash")
        interviewer = Staff(permissions=2, full_name="Wrong Wrongovich", email="wrong@mail.mail",
                            password_hash="hash_again")

        with app.app_context():
            db.session.add(manager1)
            db.session.add(manager2)
            db.session.add(interviewer)
            db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'test@mail.mail',
                'password_hash': 'hash'
            })

            self.token = token_info.get_json()["token"]

    def tearDown(self):
        with app.app_context():
            db.session.query(Staff).delete()
            db.session.commit()

    def test1_list(self):
        """
        Test function that gets managers list
        """
        with app.test_client() as a:
            res = a.get('/managers', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            staff_data = data["staff"]

            for i in staff_data:
                if i['staff_id'] == 1:
                    assert i['full_name'] == "Test Testov"
                elif i['staff_id'] == 2:
                    assert i['full_name'] == "Testina Testova"
                else:
                    # There should be only 2 managers, no more
                    assert False

    def test2_info(self):
        """
        Test function that gets info about a particular manager
        """
        with app.test_client() as a:
            res = a.get('/managers/1/info', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data["full_name"] == "Test Testov"

    def test3_info_restrictions(self):
        """
        Test that no other manager can get information about other managers except himself
        """
        with app.test_client() as a:
            res = a.get('/managers/3/info', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

    def test4_new_manager(self):
        """
        Test new manager creation
        """
        with app.test_client() as a:
            res = a.post('/managers/new', json={
                'full_name': 'New New',
                'email': 'new@mail.mail',
                'password_hash': 'newhash'
            }, query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0
            assert data["id"] == 4

            manager = Staff.query.filter_by(id=4).first()

            assert manager.full_name == "New New"
            assert manager.email == "new@mail.mail"
            assert manager.password_hash == "newhash"

    def test5_new_manager_restriction(self):
        """
        Test that a manager is not created when not full information is provided
        """
        with app.test_client() as a:
            res = a.post('/managers/new', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            manager = Staff.query.filter_by(id=4).first()

            assert manager is None

            # Incomplete call - no login
            res = a.post('/managers/new', json={
                'full_name': 'New New',
                'password_hash': 'newhash'
            })

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            manager = Staff.query.filter_by(id=4).first()

            assert manager is None

            # duplicate email
            res = a.post('/managers/new', json={
                'full_name': 'Another One',
                'email': 'test@mail.mail',
                'password_hash': 'bitesthedust'
            }, query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            manager = Staff.query.filter_by(id=4).first()

            assert manager is None

    def test6_delete(self):
        """
        Test deletion of a manager
        """
        with app.test_client() as a:
            res = a.get('/managers/2/delete', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            manager = Staff.query.filter_by(id=2).first()

            assert manager is None

    def test7_delete_restrictions(self):
        """
        Test that managers cannot delete other managers
        """
        with app.test_client() as a:
            res = a.get('/managers/3/delete', query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] < 0
            assert len(data["error_message"]) != 0

            manager = Staff.query.filter_by(id=3).first()

            assert manager.full_name == "Wrong Wrongovich"
            assert manager.email == "wrong@mail.mail"
            assert manager.password_hash == "hash_again"


if __name__ == "__main__":
    unittest.main()

import unittest

from backend_application import *
from backend_application.model import *
from backend_application.token import *


class InterviewersTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(InterviewersTests, cls).setUpClass()

        app.config.from_object('backend_application.app_config.TestingConfig')

        db.init_app(app)

        with app.app_context():
            db.drop_all()
            db.create_all()

    @classmethod
    def tearDownClass(cls):
        super(InterviewersTests, cls).tearDownClass()

        with app.app_context():
            db.drop_all()

    def setUp(self):
        program1 = Program(program_name="Test program")
        program2 = Program(program_name="Another test program")

        staff1 = Staff(permissions=1, full_name="Roxy Swadling", email="rswadling0@chronoengine.com",
                       password_hash="jSrdkCV")

        db.session.add(program1)
        db.session.add(staff1)
        db.session.commit()

        with app.test_client() as a:
            token_info = a.post('/staff/login', json={
                'email': 'rswadling0@chronoengine.com',
                'password_hash': 'jSrdkCV'
            })

            self.token = token_info.get_json()["token"]

    def tearDown(self):
        db.session.query(Staff).delete()
        db.session.query(Program).delete()
        db.session.commit()

    def test1_list(self):
        '''
        Test function that gets programs list
        '''
        with app.test_client() as a:
            res = a.get("/programs", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            program_data = data['programs']

            for i in program_data:
                if i['program_id'] == 1:
                    assert i['program_name'] == "Test program"
                elif i['program_id'] == 2:
                    assert i['program_name'] == "Another test program"
                else:
                    # There should be only 2 programs, no more
                    assert False

    def test2_info(self):
        '''
        Test function that gets information about a particular program
        '''
        with app.test_client() as a:
            res = a.get("/programs/1/info", query_string=dict(token=self.token))

            assert res.is_json

            data = res.get_json()
            assert data["status"] == 0
            assert len(data["error_message"]) == 0

            assert data["program_name"] == "Test program"


if __name__ == "__main__":
    unittest.main()

#!/usr/bin/env sh

export FLASK_APP=application.py
export FLASK_ENV=development
# export FLASK_ENV=production

flask "$@"
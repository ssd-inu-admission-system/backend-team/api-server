# API Server

Installation - clone the repository, run **./db_create.py** to setup the database and then **./run.sh run** to start the development server.

Testing - run **python -m unittest discover -f**

Mock data - after the database creation, run **./db_mock.py**